package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

//mark this Java object as a database table
@Entity
//name the table
@Table(name = "posts")
public class Post {
    //indicate the primary key
    @Id
    //auto-increment the ID column
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column String content;

    //default constructor needed when retrieving posts
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }


}
