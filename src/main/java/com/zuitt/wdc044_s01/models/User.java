package com.zuitt.wdc044_s01.models;

import javax.persistence.*;


@Entity

@Table(name = "user")
public class User {

    @Id

    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column String password;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setContent(String content){
        this.password = password;
    }


}

